#include <vector>
#include <random>

namespace cpstl {
  namespace utils {
/**
 * Generate a random vector from start <= size
 */
template <typename T>
std::vector<T> generate_random_vector(std::size_t size = 100000, std::size_t start = 0) {
  std::vector<T> tmp;
  tmp.reserve(size);

  std::random_device rd;
  std::mt19937_64 mt(rd());
  auto generator = std::uniform_real_distribution<float>(start, size);

  for (std::size_t index = 0; index < size; index++)
    tmp.push_back(generator(mt));

  return tmp;
}
  };

};
