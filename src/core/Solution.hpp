/**
 * Competitive-Programming-and-Contests-VP-Solution a collection of
 * code with an engineering approach to solve the problem.
 * https://github.com/vincenzopalazzo/Competitive-Programming-and-Contests-VP-Solution
 *
 * Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <cstdlib>
#include <string>
#include <vector>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>

#include <ff/ff.hpp>
#include <ff/parallel_for.hpp>
#include <ff/visit/str_visitor.hpp>

#include "../test/Utils.hpp"

// FIXME: This log implementation it is not thread safe
// because it is implemented as competitive programming problem.
const cpstl::Log LOG(true);

/**
 * Function implementation to use the fast flow map implementation.
 */
template <typename T>
std::vector<T> ff_map(std::vector<T> &input, std::size_t core, std::size_t chunc) {
  ff::ParallelFor map(core);
  map.parallel_for(0L, input.size(), [&input](T const item){
    std::this_thread::sleep_for (std::chrono::milliseconds(1));
    cpstl::cp_log(LOG, "Finish run...");
  });

  ff::ff_str_visitor visit;
  map.accept(visit);
  cpstl::cp_log(LOG, visit.get_result());
  return input;
}
