cmake_minimum_required(VERSION 3.9)
project(ff_benchmarks)

set(CMAKE_CXX_STANDARD 14)

find_package(benchmark REQUIRED)

add_executable(
        ${PROJECT_NAME}
        Main.cpp
)

target_link_libraries(${PROJECT_NAME} PRIVATE benchmark::benchmark)
target_include_directories(${PROJECT_NAME}  PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/fastflow)
target_link_libraries(${PROJECT_NAME} PUBLIC pthread)
