/**
 * Competitive-Programming-and-Contests-VP-Solution a collection of
 * code with an engineering approach to solve the problem.
 * https://github.com/vincenzopalazzo/Competitive-Programming-and-Contests-VP-Solution
 *
 * Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <benchmark/benchmark.h>
#include <cmath>
#include <thread>
#include "../src/core/Solution.hpp"
#include "../src/core/Utils.hpp"

const int MACHINE_CORES = std::thread::hardware_concurrency();

static void BM_FF_PAR_FOR(benchmark::State& state)
{
    for(auto _ : state) {
        state.PauseTiming();
        auto vect = cpstl::utils::generate_random_vector<int>(state.range(0));
        state.ResumeTiming();
        ff_map(vect, MACHINE_CORES, 0);
    }
}

static void BM_FF_PAR_FOR_SINGLE(benchmark::State& state)
{
  for(auto _ : state) {
    state.PauseTiming();
    auto vect = cpstl::utils::generate_random_vector<int>(state.range(0));
    state.ResumeTiming();
    ff_map(vect, 1, 0);
  }
}

BENCHMARK(BM_FF_PAR_FOR)->Range(8, 8 << 12);
BENCHMARK(BM_FF_PAR_FOR_SINGLE)->Range(8, 8 << 12);
BENCHMARK_MAIN();

